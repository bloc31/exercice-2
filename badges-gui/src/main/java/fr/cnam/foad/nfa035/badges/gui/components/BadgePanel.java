package fr.cnam.foad.nfa035.badges.gui.components;

import javax.swing.*;
import java.awt.*;

public class BadgePanel extends JPanel {

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            dao.getBadgeFromMetadata(bos, badge);
            g.drawImage(ImageIO.read(new ByteArrayInputStream(bos.toByteArray())), 0, 0, this.getWidth(), this.getHeight(), this);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

}
